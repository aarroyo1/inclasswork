/**********************************************
* File: DFS.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*
* Edits made by Annalise Arroyo to solve Prompt 2 of Coding Assignment 4
* 
* Contains an STL implementation of an Adjacency List
* graph Depth-First Search (no directions or weights) using C++ STL Vector 
*
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ g++ -g -std=c++11 -Wpedantic DFS.cpp -o DFS
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ ./DFS

Output may be found at DFS.out. Can be generated using ./DFS > DFS.out 

**********************************************/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <stack>
#include <fstream>

#define ROOT 0

// data structure to store graph edges
template<class T>
struct Edge {
	T src;
	T dest;
	int weight;
	
   template <typename U>      // all instantiations of this template are friends
   /********************************************
   * Function Name  : operator<<
   * Pre-conditions :  std::ostream&, const Edge<U>& 
   * Post-conditions: friend std::ostream&
   * Overloaded Friend Operator<< to print an edge  
   ********************************************/
   friend std::ostream& operator<<( std::ostream&, const Edge<U>& );

};

template <typename T>
std::ostream& operator<<( std::ostream& os, const Edge<T>& theEdge) {
   
   std::cout << "{" << theEdge.src << " " << theEdge.dest << " " << theEdge.weight << "} ";
   
   return os;
}


template<class T>
struct Vertex {
	T value;
	mutable std::vector< Edge <T> > edges;
	
	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded == operator to compare vertex value  
	********************************************/
	bool operator==(const Vertex<T>& rhs) const{
		
		return value == rhs.value;
		
	}
	
	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded < operator to compare vertex values
	********************************************/
	bool operator<(const Vertex<T>& rhs) const{
		
		return value < rhs.value; 
		
	}
};

// class to represent a graph object
template<class T>
class Graph
{
	public:
		// construct a vectors to represent an adjacency list
		std::vector< Vertex<T> > adjList;
		
		// Hash Table to correlate Verticies with location in vector
		std::unordered_map< T, int > hashVerts;

		/********************************************
		* Function Name  : Graph
		* Pre-conditions : const std::vector< Edge<T> > &edges
		* Post-conditions: none
		*  
		* Graph Constructor
		********************************************/
		Graph(std::vector< Edge<T> > &edges, bool directional)
		{

			// add edges to the directed graph
			for (auto &edge: edges)
			{

				// Insert Origin 
				addEdge(edge);
				
				// Since this is undirected, put in opposite
				// Uncomment if graph is directional
				// Create a temp edge to reverse origin and destination 
				if(!directional){
					Edge<T> tempEdge = {edge.dest, edge.src, edge.weight};
					addEdge(tempEdge);
				}
				
			}
		}
		
		/********************************************
		* Function Name  : addEdge
		* Pre-conditions : Edge<T> edge
		* Post-conditions: none
		*  
		* Insert an edge into the graph
		********************************************/
		void addEdge(const Edge<T>& edge){
			
			// Create a temporary vertex with the source
			Vertex<T> tempVert;
			tempVert.value = edge.src;
			
			// Element was not found
			if(hashVerts.count(tempVert.value) == 0){
				
				// HashWords.insert( {wordIn, 1} );
				hashVerts.insert( {tempVert.value, adjList.size()} );
				
				// Inset the edge into the temporary vertex 
				// Serves as the first edge 
				tempVert.edges.push_back(edge);
				
				// Insert the vertex into the set 
				adjList.push_back(tempVert);
				
			}
			// Element was found!
			else{
				
				// Use the hash to get the location in adjList, then push onto the edges vector 
				adjList.at(hashVerts[tempVert.value]).edges.push_back(edge);
				
			}
		}
		
		/********************************************
		* Function Name  : returnHashLocation
		* Pre-conditions : T value
		* Post-conditions: int
		* 
		* Returns the location in the graph of the requested value 
		********************************************/
		// In Class Code Goes Here 
		int returnHashLocation(T value){
			
			return hashVerts[value];
		}
 
};

/********************************************
* Function Name  : printGraph
* Pre-conditions : const Graph<T>& graph
* Post-conditions: none
*  
* Prints all the elements in the graph 
********************************************/
template<typename T>
void printGraph(const Graph<T>& graph){
	
	std::cout << "Number of buckets is: " << graph.adjList.size() << std::endl;
	std::cout << "Origin: {Destination, Weight}" << std::endl;

	for(int iter = 0; iter < graph.adjList.size(); iter++){
		
		std::cout << graph.adjList.at(iter).value << ": ";
		
		for(int jter = 0; jter < graph.adjList.at(iter).edges.size(); jter++){
			
			std::cout << graph.adjList.at(iter).edges.at(jter);
			
		}
		
		std::cout << std::endl;
	}
	
	std::cout << std::endl;
	
}

/********************************************
* Function Name  : GraphDFS
* Pre-conditions : Graph<T>& graph, std::stack<T>& DFSOrder, T& findElem
* Post-conditions: none
* 
* Recursive basic function for DFS Traversal
* Stores traversal results in DFSOrder 
********************************************/
template<class T>
void GraphDFS(Graph<T>& graph, std::stack< T >& DFSOrder, T& findElem, T& value){
	
	// If the Graph has no elements, the return false 
	if(graph.adjList.size() == 0)
		return;
	
	// Create a Hash for visited Vertices 
	std::unordered_map<T, bool> visitedVerts;

    int start = -1;
    for (int k = 0; k < graph.adjList.size(); k++){
        if (graph.adjList.at(k).value == value){
            std::cout<< "Root value found." << std::endl;
            start = k;
        }
    }
    if (start == -1){
        std::cout << "Unable to find Root Value" << std::endl;
        return;
    }
	
	// If this returns true, then the element was found, add the root
	if(GraphDFS(graph, start, DFSOrder, visitedVerts, findElem)){
		std::cout << "The path exists" << std::endl;
		DFSOrder.push(graph.adjList.at(start).value);
	}
	
}


/********************************************
* Function Name  : GraphDFS
* Pre-conditions : Graph<T>& graph, int vecLoc, std::stack<T>& DFSOrder, 
*				   std::unordered_map<T, bool>& visitedVerts
* Post-conditions: none
*  
********************************************/
template<class T>
bool GraphDFS(Graph<T>& graph, int vecLoc, std::stack< T >& DFSOrder, 
	std::unordered_map<T, bool>& visitedVerts, T& findElem){
		
	// Mark the root node as visited 
	visitedVerts.insert( { graph.adjList.at(vecLoc).value, true } );
		
	// Get the vertex based on vecLoc 
	Vertex<T>* tempVert = &graph.adjList.at(vecLoc);
	
	// Base Case - Found node 
	if(tempVert->value == findElem){
		
		// No need to push onto the stack since the recursive call 
		// pushes all the found vertices.
		// Only need to push to the stack for the edge destination below
		
		return true;
	}
	
	for(int iter = 0; iter < tempVert->edges.size(); iter++){
		
		T tempDest = tempVert->edges.at(iter).dest;

		// If the edge destination is the element we are looking for 
		// Push onto the stack, and then return to the previous recursive call  
		if(tempDest == findElem){
			DFSOrder.push( tempDest );
			return true;
		}
		
		// First, check if the destination is in the Hash Table 
		// If not, that means its a leaf in a directed graph.
		// Failed the previous check, so it is not the sought destination 
		// Return false 
		if(graph.hashVerts.count( tempDest ) == 0){
			return false;
		}
		
		// Otherwise, the path continues
		if( visitedVerts.count( tempDest ) == 0 ){
			
			// Mark the destination vertex from the edge as visited 
			visitedVerts.insert( { tempDest, true } );
			
			// The recursive call found the node, then push current destination onto the stack 
			if(GraphDFS( graph, graph.returnHashLocation( tempDest ), DFSOrder, visitedVerts, findElem)){
				
				// If the edge destination is the element we are looking for 
				// Push onto the stack, and then return to the previous recursive call 
				DFSOrder.push( tempDest );
				return true;
			}
		}
	}
	
	return false;
	
}

/********************************************
* Function Name  : printDFS
* Pre-conditions : std::stack< T > DFSOrder
* Post-conditions: none
* 
* Prints the results of a DFS Traversal 
********************************************/
template<class T>
void printDFS(std::stack< T >& DFSOrder, T& findElem){
	
	std::cout << "The Depth-First Search Order to find " << findElem << " is: ";
	
	// Inform the user if the element was not found 
	if(DFSOrder.empty()){
		
		std::cout << findElem << " was not found in the graph.";
	}
	
	// Stack is not empty -> Element was found 
	else{
			
		while(!DFSOrder.empty()){
			
			// Print the element at the top 
			std::cout << DFSOrder.top() << " ";
			
			// Remove the element from the stack 
			DFSOrder.pop();
			
		}
	
	}
	
	std::cout << std::endl << std::endl;
	
}

template< class T >
/********************************************
* Function Name  : runDFS
* Pre-conditions : T findElem, Graph<T>& theGraph, std::stack< T >& DFSOrder
* Post-conditions: none
* 
* Runs and prints the results to the user 
********************************************/
void runDFS(T findElem, Graph<T>& theGraph, std::stack< T >& DFSOrder, T value){
	
	GraphDFS(theGraph, DFSOrder, findElem, value);
	printDFS(DFSOrder, findElem);
	
}


/********************************************
* Function Name  : testCharDFS
* Pre-conditions : Graph<char>& charGraph
* Post-conditions: none
* 
* Tests the developed character Graph 
********************************************/
/*
void testCharDFS(Graph<char>& charGraph){

	// print adjacency list representation of graph
	printGraph(charGraph);
	std::stack< char > charDFSOrder;
	
	runDFS('N', charGraph, charDFSOrder);
	
	runDFS('O', charGraph, charDFSOrder);
	
	runDFS('T', charGraph, charDFSOrder);
	
	runDFS('R', charGraph, charDFSOrder);
	
	runDFS('E', charGraph, charDFSOrder);
	
	runDFS('D', charGraph, charDFSOrder);
	
	// Test for an element not in the graph 
	runDFS('Q', charGraph, charDFSOrder);

}
*/

/********************************************
* Function Name  : testIntDFS
* Pre-conditions : Graph<int>& intGraph
* Post-conditions: none
* 
* Runs the int graph 
********************************************/
/*
void testIntDFS(Graph<int>& intGraph){

	// print adjacency list representation of graph
	printGraph(intGraph);
	
	// Get DFS Order of the edges 
	std::stack< int > intDFSOrder;
	
	runDFS(0, intGraph, intDFSOrder);
	
	runDFS(1, intGraph, intDFSOrder);
	
	// 1842 is not in graph. Test for not found 
	runDFS(1842, intGraph, intDFSOrder);
	
	runDFS(2, intGraph, intDFSOrder);
	
	runDFS(3, intGraph, intDFSOrder);

	runDFS(4, intGraph, intDFSOrder);
	
}
*/
/********************************************
* Function Name  : classRun
* Pre-conditions : Graph<int>& classGraph
* Post-conditions: none
* 
* Runs DFS for the graph shown in lecture 
********************************************/
/*
void classRun(Graph<int>& classGraph){

	// print adjacency list representation of graph
	printGraph(classGraph);
	
	// Get DFS Order of the edges 
	std::stack< int > classDFSOrder;

	runDFS(18, classGraph, classDFSOrder);
	
	runDFS(30, classGraph, classDFSOrder);
	
	runDFS(20, classGraph, classDFSOrder);
	
	runDFS(4, classGraph, classDFSOrder);
	
	runDFS(17, classGraph, classDFSOrder);
	
	runDFS(10, classGraph, classDFSOrder);

}
*/
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* Main Driver Function 
********************************************/
int main(int argc, char** argv)
{
	// Run the DFS shown in class 
	// Allocate the edges as shown in the lecture slides
	std::vector< Edge<int> > edgeClass =
	{
		{4, 20, 1}, {4, 5, 1}, 
		{20, 10, 1}, {20, 30, 1},
		{5, 10, 1}, 
		{10, 18, 1}, 
		{30, 18, 1}
	};
    Graph<int> testGraph1(edgeClass, true);
    std::stack< int > classDFSOrder;
    printGraph(testGraph1);
    int toFind = 4;
    int beginAt = 4;

    std::cout << "Test 1:" << std::endl;
    std::cout << "Root: " << beginAt << "\t Path to: " << toFind << std::endl;
    runDFS(toFind, testGraph1, classDFSOrder, beginAt); 
    
    toFind = 18;
    std::cout << "------------------" << std::endl;
    std::cout << "Test 2:" << std::endl;
    std::cout << "Root: " << beginAt << "\t Path to: " << toFind << std::endl;

    runDFS(toFind, testGraph1, classDFSOrder, beginAt);
    
    toFind = 10;

    std::cout << "------------------" << std::endl;
    std::cout << "Test 3:" << std::endl;
    std::cout << "Root: " << beginAt << "\t Path to: " << toFind << std::endl;

    runDFS(toFind, testGraph1, classDFSOrder, beginAt);

    toFind = 4;
    beginAt = 10;

    std::cout << "------------------" << std::endl;
    std::cout << "Test 4:" << std::endl;
    std::cout << "Root: " << beginAt << "\t Path to: " << toFind << std::endl;

    runDFS(toFind, testGraph1, classDFSOrder, beginAt);


	std::cout << "-------------------------------------" << std::endl;
	
    
	std::vector< Edge<char> > edges =
	{
		{'N', 'O', 1}, {'N', 'T', 1}, {'O', 'R', 1}, 
		{'O', 'E', 1}, {'T', 'R', 1}, {'R', 'D', 1}, 
		{'E', 'D', 1} 
	};

    Graph<char> testGraph2(edges, true);
    std::stack < char > charDFStest;
    printGraph(testGraph2);
    
    char startHere = 'N';
    char findMe = 'R';

    std::cout << "Test 1:" << std::endl;
    std::cout << "Root: " << startHere << "\t Path to: " << findMe << std::endl;
    
    runDFS(findMe, testGraph2, charDFStest, startHere);

    startHere = 'O';
    findMe = 'D';
    std::cout << "-----------------" << std::endl;
    std::cout << "Test 2:" << std::endl;
    std::cout << "Root: " << startHere << "\t Path to: " << findMe << std::endl;
    
    runDFS(findMe, testGraph2, charDFStest, startHere);

    startHere = 'T';
    findMe = 'N';

    std::cout << "-----------------" << std::endl;
    std::cout << "Test 2:" << std::endl;
    std::cout << "Root: " << startHere << "\t Path to: " << findMe << std::endl;
    
    runDFS(findMe, testGraph2, charDFStest, startHere);

	return 0;
}
