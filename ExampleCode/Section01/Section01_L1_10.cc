/****************************************
 * File name: Section01_L1_10.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu 
 * 
 * This file creates a random number of elements 
 * to put in a queue that is implemented using two 
 * stacks, and shows the push, pop, front, and 
 * back are correctly designed.
 * *************************************/

#include "Section01_stack_queue.h"

#include <iostream>
#include <cstdlib>

/*********************************************
 * Function Name: main 
 * Preconditions: none 
 * Postconditions: int  
 * This is the main driver function for the 
 * program 
 * ******************************************/ 
int main(int argc, char *argv[]) {
    
    // Create a new stack_queue pointer
    stack_queue<int>* the_queue = new stack_queue<int>();
    
    // Seed the random number generatpr
    srand(time(NULL));

    // Randomly generate the number of elements <= 20
    // Add 1 so there is no 0 total
    size_t queue_size = rand() % 20 + 1;
    std::cout << "Input " << queue_size << " values on queue:\t";
    
    // Generate random values between 0 and 100
    for(size_t iter = 0; iter < queue_size; iter++){
        the_queue->push(rand() % 100);
        std::cout << the_queue->back() << ", ";
    }
    std::cout << std::endl;
    
    // Next, pop all the elements to prove it works
    std::cout << "Elements popped off queue :\t";
    for(size_t iter = 0; iter < queue_size; iter++){
        std::cout << the_queue->front() << ", ";
        the_queue->pop();
    }
    std::cout << std::endl;    

    return 0;
}
