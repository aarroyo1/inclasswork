/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
#include <map>
#include <unordered_map>
#include <iterator>
#include <iostream>
#include <string>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv){

    std::map<std::string, int> ageHashOrdered = {{"Matthew", 38}, {"Alfred", 72}, {"Rosco", 36}, {"James", 35}};

    std::map<std::string, int>::iterator iterOr;

    std::cout << "The ordered hashes are: " << std::endl;

    for(iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++){
        std::cout << iterOr->first << " " << iterOr->second << std::endl;
    }

    std::unordered_map<std::string, int> ageHashUnordered = { {"Matthew", 38}, {"Alfred", 72}, {"Rosco", 36}, {"James", 35}};
    
    std::unordered_map<std::string, int>::iterator iterUn;
    
    std::cout << "------" << std::endl << "The Unordered Hashes are: " << std::endl;

    for(iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++){
        std::cout << iterUn->first << " " << iterUn-> second << std::endl;
    }

    std::cout << "The Ordered Example: " << ageHashOrdered["Matthew"] << std::endl;
    std::cout << "The Unordered Example: " << ageHashUnordered["James"] << std::endl;

	return 0;
}
