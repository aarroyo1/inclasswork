/**********************************************
 * ArrayPairSum
 * Author: Annalise Arroyo
 * Email: aarroyo1@nd.edu
 * 3/31/2019
 *
 * This program finds all pairs of integers within an array that wum to an inputted value.
 * ********************************************/

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <string>
#include "SeparateChaining.h"

/****************************
 * Function Name:   printVector
 * Pre-conditions:  std::vector<std::string>
 * Post-conditions: void
 *
 * Prints out the vector created in the main function.
 * *************************/
void printVector(std::vector<std::string> V);

/****************************
 * Function Name:   main
 * Pre-conditions:  int, char**
 * Post-conditions: int
 *
 * Main function for finding the pairs of integers.
 * **************************/
int main(int argc, char** argv) {
    
    int input = std::stoi(argv[1]);
    int array [6] = {0, 2, 3, 3, 1, 4};
    int hashsize = input + 1; 

    HashTable<int> hTable(hashsize);

    for (int i = 0; i <= 5; i++){
        hTable.insert(array[i]);
    }
    
    hTable.printHash(std::cout);
    
    int iterlength = input/2;
    int paircount = 0;
    std::vector<std::string> pairs;

    for(int iter = 0; iter <= iterlength; iter++){
        if (hTable.contains(input - iter) && hTable.contains(iter)){
            int La = hTable.getLength(iter);
            int Lb = hTable.getLength(input-iter);
            paircount = paircount + La*Lb;              //The multiplication of the lengths accounts for duplicates in the array.
            std::string vInput = "(" + std::to_string(iter) + ", " + std::to_string(input-iter) + "), with frequency " + std::to_string(La*Lb);
            pairs.push_back(vInput);
        }
        else
            std::cout << "The pair (" << std::to_string(iter) << ", " << std::to_string(input-iter) << ") is not in the array." << std::endl;
    }   
    
    std::cout << "There are " << paircount << " pairs that add up to " << input << " in the array." << std::endl;
    
    printVector(pairs);
    return 0;
}

void printVector(std::vector<std::string> V) {
    int length = V.size();
    std::cout << "The pairs found are: " << std::endl;
    for(int i = 0; i < length; i++){
        std::cout << V[i] << std::endl;
    }
}

