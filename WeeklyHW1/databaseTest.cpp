#include <iostream>
/****************************
 *File: databaseTest.cpp
 *Author: Annalise Arroyo
 *Email: aarroyo1@nd.edu
 *
 * This is the driver function to build the database and test it
 *
 ****************************/
#include <ctime>
#include <cstdlib>
#include "BTree.h"

/********************************
 * struct name: Data
 * 
 * This is the struct for the nodes in the database
 * ******************************/
struct Data{
	int data;
	int key;

	Data(int d, int k) : data(d), key(k) {}
    Data() : data(0), key(0) {}
	bool operator>(const Data& rhs) const{
		if (key > rhs.key)
			return true;
		return false;
	}
	bool operator<(const Data& rhs) const{
		if (key < rhs.key)
			return true;
		return false;
	}
	bool operator==(const Data& rhs) const{
		if (key == rhs.key)
			return true;
		return false;
	}

    friend std::ostream& operator<<(std::ostream& outStream, const Data& printData);
};

/****************************
 *Function name: operator<<
 * Pre-conditions: ostream&, const Data&
 * Post-conditions: ostream&
 * 
 * This function prints out the Data object.
 * **************************/
std::ostream& operator<<(std::ostream& outStream, const Data& printData){
    outStream<< "key = " <<printData.key << "\t" << "data = " << printData.data << std::endl;
    return outStream;
}

/*****************************
 * Function name: main
 * Pre-conditions: none
 * Post-conditions: int
 *
 * This is the main driver function for the program
 * ***************************/
int main() {
	BTree<Data> Database(4);

    srand(time(0));
    int i = 0;
    while (i < 1000) {
        int k = (rand() % 1000) + 1;             //Using a range from 0 - 1000 for fun
        int d = (rand() % 1000) + 1;
        Data insertMe(d, k);
        Database.insert(insertMe);
        i++;
    }
	std::cout << "Current Tree is \n";
	//Database.traverse();                      //This is commented out so that 1000 lines arent printed to the terminal.
	std::cout << std::endl;

    for(int a = 0; a < 10; a++) {
        int searchKey = (rand() % 1000) + 1;
        std::cout << "Searching tree for key: " << searchKey << std::endl;
        Data searchMe(0, searchKey);
        BTreeNode<Data> * result = Database.search(searchMe);
        if(result==NULL)
            std::cout << "The key " << searchKey << " is not in the tree." << std::endl;
        else {
            std::cout << "The key " << searchKey << " is in the tree." << std::endl;
            int nKeys = result->numKeys;
            int n = 0;
            while (n <=nKeys) {
                if(searchKey == result->keys[n].key)
                    std::cout << result->keys[n];
                n++;
            }
        }
        std::cout << std::endl;                 //for readability
    }

	return 0;	
}
